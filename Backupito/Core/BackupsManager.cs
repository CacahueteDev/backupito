﻿using System.Formats.Tar;
using System.Text.Json;
using Backupito.Config;
using Backupito.Utilities;

namespace Backupito.Core;

public static class BackupsManager
{
    public static List<StateBackupEntry> Backups { get; private set; } = new();
    
    public static void LoadBackups()
    {
        string stateFilePath = $"{LocalStorage.CurrentConfig.BackupDestinationPath}/state.json";

        if (!File.Exists(stateFilePath))
        {
            File.WriteAllText(stateFilePath, "[]");
            return;
        }

        Backups = JsonSerializer.Deserialize<StateBackupEntry[]>(
            File.ReadAllText(stateFilePath),
            new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower})!.ToList();

        Backups.Sort((left, right) => left.CreationDate.CompareTo(right.CreationDate));
    }

    public static string[] CleanOldBackups()
    {
        int delta = Backups.Count - LocalStorage.CurrentConfig.BackupMaxCount;
        if (delta <= 0) return [];

        List<string> removedBackups = new();
        
        for (int i = 0; i < delta; i++)
        {
            Delete(Backups[i].Id);
            removedBackups.Add(Backups[i].Id);
        }

        return removedBackups.ToArray();
    }

    public static void SaveBackups()
    {
        string stateFilePath = $"{LocalStorage.CurrentConfig.BackupDestinationPath}/state.json";

        File.WriteAllText(stateFilePath, JsonSerializer.Serialize(Backups.ToArray(),
            new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower}));
    }

    public static StateBackupEntry? Get(string id)
        => Backups.FirstOrDefault(backup => backup.Id.ToLower().StartsWith(id.ToLower()));

    public static void Delete(string id)
    {
        StateBackupEntry? entry = Get(id);
        if (entry == null) return;
        
        Directory.Delete(entry.Path, true);
        Backups.Remove(entry);
    }

    public static StateBackupEntry CreateBackup(string backupPath, bool useImatId)
    {
        string id = useImatId ? IdGenerator.GenerateImat() : IdGenerator.Generate();
        StateBackupEntry newBackupEntry = new()
        {
            CreationDate = DateTime.Now,
            Id = id,
            InitialPath = backupPath,
            Path = $"{LocalStorage.CurrentConfig.BackupDestinationPath}/{id}"
        };
        
        EnsureDirectory(newBackupEntry.Path);
        
        TarFile.CreateFromDirectory(backupPath, 
            $"{newBackupEntry.Path}/backup.tar.gz", false);
        
        Backups.Add(newBackupEntry);

        return newBackupEntry;
    }
}