﻿namespace Backupito.Core;

public static class GlobalMethods
{
    public static void EnsureDirectory(string path)
    {
        if (!OperatingSystem.IsLinux())
            path = path.TrimStart('/');

        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
    }
}