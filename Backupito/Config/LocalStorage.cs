﻿using System.Text.Json;

namespace Backupito.Config;

public static class LocalStorage
{
    private static string _configPath = "/etc/backupito/config.json";

    public static ConfigFile? CurrentConfig { get; private set; }

    static LocalStorage()
    {
        if (!OperatingSystem.IsLinux())
            _configPath = _configPath.TrimStart('/');
    }

    static void CreateDefaultConfig()
    {
        File.WriteAllText(_configPath, JsonSerializer.Serialize(ConfigFile.Default,
            new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower}));
    }

    public static void Load()
    {
        EnsureDirectory("/etc/backupito");
        EnsureDirectory("/var/log/backupito");
        
        if (!File.Exists(_configPath)) CreateDefaultConfig();
        CurrentConfig = JsonSerializer.Deserialize<ConfigFile>(File.ReadAllText(_configPath), 
            new JsonSerializerOptions {PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower});

        EnsureDirectory(CurrentConfig.BackupDestinationPath);
    }
}