using System.Formats.Tar;
using System.Globalization;
using System.Text.Json.Serialization;

namespace Backupito.Config;

public class StateBackupEntry
{
    public string Id { get; set; }
    public DateTime CreationDate { get; set; }
    public string InitialPath { get; set; }
    public string Path { get; set; }
    [JsonIgnore] public string ArchivePath => $"{Path}/backup.tar.gz";

    public override string ToString() => $" - {Id}: at " +
                                         $"{CreationDate.ToString("f", CultureInfo.InvariantCulture)} backup " +
                                         $"{InitialPath}";

    public bool Restore()
    {
        if (!File.Exists(ArchivePath)) return false;
        
        TarFile.ExtractToDirectory(ArchivePath, InitialPath, true);
        
        return true;
    }
}