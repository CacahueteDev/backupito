﻿namespace Backupito.Config;

public class ConfigFile
{
    public static ConfigFile Default => new()
    {
        BackupPaths = Array.Empty<string>(),
        BackupDestinationPath = "/var/backup",
        BackupMaxCount = 10
    };
    
    public string[] BackupPaths { get; set; }
    public string BackupDestinationPath { get; set; }
    public int BackupMaxCount { get; set; }
}