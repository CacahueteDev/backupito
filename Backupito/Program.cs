﻿using Backupito.Config;
using Backupito.Core;
using Backupito.Subcommands;
using Backupito.Utilities;

LocalStorage.Load();
BackupsManager.LoadBackups();
Subcommand[] subcommands =
[
    new BackupSubcommand(),
    new ListSubcommand(),
    new InfoSubcommand(),
    new RestoreSubcommand(),
    new DeleteSubcommand(),
    new CleanSubcommand()
];

void ShowHelp()
{
    Console.WriteLine("usage: backupito [subcommand] ...");
    Console.WriteLine("subcommands:");
    
    foreach (Subcommand sc in subcommands)
    {
        Console.WriteLine($"{$"{sc.Id} {sc.Arguments}",-15}: {sc.Description}");
    }
}

ArgumentsParser argsParser = new(args);
string? subcommand = argsParser.Get(0);

foreach (Subcommand command in subcommands)
{
    if (command.Id == subcommand)
    {
        command.Run(argsParser.Offset(1));
        return;
    }
}

if (subcommand == null || argsParser.Contains("help")) ShowHelp();
else Logging.Error($"No subcommand named '{subcommand}' has been found");