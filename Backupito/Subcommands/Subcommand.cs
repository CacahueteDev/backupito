﻿namespace Backupito.Subcommands;

public abstract class Subcommand
{
    public abstract string Id { get; }
    public virtual string Arguments => string.Empty;
    public abstract string Description { get; }

    public abstract void Run(ArgumentsParser parser);
}