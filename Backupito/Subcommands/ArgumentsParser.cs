﻿namespace Backupito.Subcommands;

public class ArgumentsParser(string[] args) : List<string>(args)
{
    public ArgumentsParser Offset(int count)
        => new ArgumentsParser(args.Skip(count).ToArray());

    public void Dump()
    {
        Console.WriteLine(string.Join(", ", this));
    }

    public string? Get(int index)
        => index < 0 || index > Count - 1 ? null : this[index];

    public string? Get(string arg)
    {
        for (int i = 0; i < Count; i++)
        {
            if (this[i] == arg.TrimStart('-') && i < Count - 1)
                return this[i + 1];
        }

        return null;
    }
}