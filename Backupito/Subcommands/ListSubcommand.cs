﻿using System.Globalization;
using Backupito.Config;
using Backupito.Core;

namespace Backupito.Subcommands;

public class ListSubcommand : Subcommand
{
    public override string Id => "list";
    public override string Description => "list the created backups";
    
    public override void Run(ArgumentsParser parser)
    {
        foreach (StateBackupEntry backup in BackupsManager.Backups)
            Console.WriteLine(backup);
    }
}