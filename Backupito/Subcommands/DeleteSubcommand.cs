﻿using Backupito.Config;
using Backupito.Core;
using Backupito.Utilities;

namespace Backupito.Subcommands;

public class DeleteSubcommand : Subcommand
{
    public override string Id => "delete";
    public override string Description => "Deletes a backup";
    
    public override void Run(ArgumentsParser parser)
    {
        string? id = parser.Get(0);
        if (id == null)
        {
            Logging.Error("Please specify an id");
            return;
        }

        StateBackupEntry? backup = BackupsManager.Get(id);
        if (backup == null)
        {
            Logging.Error($"No backup found with matching id {id}");
            return;
        }
        
        BackupsManager.Delete(id);
        BackupsManager.SaveBackups();
        
        Logging.Info($"Deleted backup {backup.Id} successfully");
    }
}