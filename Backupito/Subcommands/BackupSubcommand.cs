﻿using Backupito.Config;
using Backupito.Core;
using Backupito.Utilities;

namespace Backupito.Subcommands;

public class BackupSubcommand : Subcommand
{
    public override string Id => "backup";
    public override string Arguments => "[--imat]";
    public override string Description => "make a backup";
    
    public override void Run(ArgumentsParser parser)
    {
        int i = 0;
        int max = LocalStorage.CurrentConfig.BackupPaths.Length;
        
        foreach (string path in LocalStorage.CurrentConfig.BackupPaths)
        {
            Logging.Info($"[{i}/{max}] Creating backup...");

            BackupsManager.CreateBackup(path, parser.Contains("--imat"));
            
            Logging.Info($"[{i}/{max}] Finished backup");
        }
        
        BackupsManager.SaveBackups();
        Logging.Info($"Finished {i}/{max} backups");
    }
}