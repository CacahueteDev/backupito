﻿using Backupito.Config;
using Backupito.Core;
using Backupito.Utilities;

namespace Backupito.Subcommands;

public class InfoSubcommand : Subcommand
{
    public override string Id => "info";
    public override string Arguments => "[id]";
    public override string Description => "gives info of a backup by id";
    
    public override void Run(ArgumentsParser parser)
    {
        string? id = parser.Get(0);
        if (id == null)
        {
            Logging.Error("Please specify an id");
            return;
        }

        StateBackupEntry? backup = BackupsManager.Get(id);
        if (backup == null)
        {
            Logging.Error($"No backup found with matching id {id}");
            return;
        }
        
        Console.WriteLine(backup);
    }
}