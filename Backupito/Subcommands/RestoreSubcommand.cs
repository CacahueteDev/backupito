﻿using Backupito.Config;
using Backupito.Core;
using Backupito.Utilities;

namespace Backupito.Subcommands;

public class RestoreSubcommand : Subcommand
{
    public override string Id => "restore";
    public override string Description => "restore a backup";
    
    public override void Run(ArgumentsParser parser)
    {
        string? id = parser.Get(0);
        if (id == null)
        {
            Logging.Error("Please specify an id");
            return;
        }

        StateBackupEntry? backup = BackupsManager.Get(id);
        if (backup == null)
        {
            Logging.Error($"No backup found with matching id {id}");
            return;
        }
        
        Logging.Info($"Restoring backup {backup.Id} to {backup.InitialPath}");
        if (backup.Restore())
        {
            Logging.Info("Completed");
            return;
        }
        
        Logging.Error("Restoring backup failed without explanation");
    }
}