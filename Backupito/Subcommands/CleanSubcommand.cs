using Backupito.Config;
using Backupito.Core;
using Backupito.Utilities;

namespace Backupito.Subcommands;

public class CleanSubcommand : Subcommand
{
    public override string Id => "clean";
    public override string Description => "Clean old backups";

    public override void Run(ArgumentsParser parser)
    {
        if (BackupsManager.Backups.Count <= LocalStorage.CurrentConfig.BackupMaxCount)
        {
            Logging.Info("Nothing to clean");
            return;
        }

        string[] deletedBackupsIds = BackupsManager.CleanOldBackups();
        BackupsManager.SaveBackups();
        Logging.Info($"Clean (deleted) backups: {string.Join(", ", deletedBackupsIds)}");
    }
}