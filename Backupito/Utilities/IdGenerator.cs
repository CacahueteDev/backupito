﻿namespace Backupito.Utilities;

public static class IdGenerator
{
    private const string Characters = "0123456789AZERTYUIOPQSDFGHJKLMWXCVBN";

    public static string Generate(int size = 8)
    {
        string final = "";

        for (int i = 0; i < size; i++)
            final += Characters[Random.Shared.Next(0, Characters.Length)];
        
        return final;
    }

    public static string GenerateImat()
    {
        string letters = new string(Characters.Where(char.IsLetter).ToArray());
        string final = "";

        for (int i = 0; i < 2; i++)
            final += letters[Random.Shared.Next(0, letters.Length)];

        final += "-";
        
        for (int i = 0; i < 3; i++)
            final += Random.Shared.Next(0, 9);
        
        final += "-";
        
        for (int i = 0; i < 2; i++)
            final += letters[Random.Shared.Next(0, letters.Length)];
        
        return final;
    }
}