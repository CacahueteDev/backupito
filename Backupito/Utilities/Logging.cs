﻿namespace Backupito.Utilities;

public static class Logging
{
    public static void Debug(string message) => Log(LoggingLevel.Debug, message);
    public static void Info(string message) => Log(LoggingLevel.Info, message);
    public static void Warning(string message) => Log(LoggingLevel.Warning, message);
    public static void Error(string message) => Log(LoggingLevel.Error, message);
    public static void Fatal(string message) => Log(LoggingLevel.Fatal, message);
    
    private static void Log(LoggingLevel level, string message)
    {
        DateTime now = DateTime.Now;
        
        Console.Write($"[{now:T}] [");
        
        switch (level)
        {
            case LoggingLevel.Debug:
                Console.ForegroundColor = ConsoleColor.Gray;
                break;
            case LoggingLevel.Info:
                Console.ForegroundColor = ConsoleColor.White;
                break;
            case LoggingLevel.Warning:
                Console.ForegroundColor = ConsoleColor.Yellow;
                break;
            case LoggingLevel.Error:
                Console.ForegroundColor = ConsoleColor.Red;
                break;
            case LoggingLevel.Fatal:
                Console.ForegroundColor = ConsoleColor.DarkRed;
                break;
        }
        
        Console.Write(level.ToString());
        Console.ResetColor();
        Console.WriteLine($"] {message}");
    }
}

public enum LoggingLevel
{
    Debug,
    Info,
    Warning,
    Error,
    Fatal
}