using Backupito.Config;
using Backupito.Core;

var builder = WebApplication.CreateBuilder(args);

LocalStorage.Load();
BackupsManager.LoadBackups();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/backups", () => BackupsManager.Backups)
    .WithName("Get All Backups")
    .WithOpenApi();

app.MapGet("/backups/{id}", (string id) => (object?)BackupsManager.Get(id) ?? Results.NotFound())
    .WithName("Get Backup #id")
    .WithOpenApi();

app.MapPost("/backup", () =>
    {
        List<StateBackupEntry> backups = new();

        foreach (string path in LocalStorage.CurrentConfig.BackupPaths)
            backups.Add(BackupsManager.CreateBackup(path, false));

        return backups;
    })
    .WithName("Create Backup")
    .WithOpenApi();

app.Run();

