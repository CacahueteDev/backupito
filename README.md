# Backupito

A program to make backups easily (written in C#).

## How to build

### Docker
Clone the repo and run the following command :
```shell
$ docker compose up
```

### Manually
Install the [dotnet sdk 8.0](https://learn.microsoft.com/fr-fr/dotnet/core/install/linux?WT.mc_id=dotnet-35129-website) (good luck with the Microsoft docs).

After dotnet is installed and is in your path variable, run these commands in the repo's root folder :
```shell
$ dotnet publish -r linux-x64 -o output
```
The binaries will be in the `output` folder.

You can now run the binary file in the `output` folder.
```shell
./output/Backupito backup
```